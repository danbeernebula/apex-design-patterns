/**
 * @author danb@nebulaconsulting.co.uk
 * @date 04/10/2018
 * @description A class can implement the Observer interface when it wants to be informed of changes in observable
 * objects. See ObservableTest class for an example.
 */

public interface IObserver {

    void notify(Observable o, Map<String, Object> arguments);

}