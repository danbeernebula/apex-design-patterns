/**
 * @author danb@nebulaconsulting.co.uk
 * @date 04/10/2018
 */

@IsTest
private class ObservableTest {

    @IsTest static void testBehavior() {

        // create the observable object
        WeatherData weather = new WeatherData();

        // check count is zero
        System.assertEquals(0, weather.countObservers());

        // check has changed
        System.assertEquals(false, weather.hasChanged());

        // set measurements with no observers
        weather.setMeasurements(100, 80, 60);

        // create observer
        CurrentConditions currCond = new CurrentConditions(weather);

        // check counts and that you can't add the same object twice
        System.assertEquals(1, weather.countObservers());
        weather.addObserver(currCond);
        System.assertEquals(1, weather.countObservers());

        // check all values are null on the observer
        System.assertEquals(null, currCond.temperature);
        System.assertEquals(null, currCond.humidity);
        System.assertEquals(null, currCond.pressure);

        // set measurement
        weather.setMeasurements(10, 20, 30);

        // check the temperature was correctly set on the observer
        System.assertEquals(10, currCond.temperature);
        System.assertEquals(20, currCond.humidity);
        System.assertEquals(30, currCond.pressure);

        // create observer
        Forecast forecast = new Forecast(weather);

        // check all values are null on the observer
        System.assertEquals(null, forecast.temperature);
        System.assertEquals(null, forecast.humidity);
        System.assertEquals(null, forecast.pressure);

        // set measurement
        weather.setMeasurements(40, 50, 60);

        // check the temperature was correctly set on the observers
        System.assertEquals(40, currCond.temperature);
        System.assertEquals(50, currCond.humidity);
        System.assertEquals(60, currCond.pressure);
        System.assertEquals(40, forecast.temperature);
        System.assertEquals(50, forecast.humidity);
        // we set the pressure to null as part of the forecast arguments
        System.assertEquals(null, forecast.pressure);

        weather.removeObserver(currCond);
        System.assertEquals(1, weather.countObservers());
        weather.clearObservers();
        System.assertEquals(0, weather.countObservers());

        // add null observer
        try {
            weather.addObserver(null);
            System.assert(false);
        } catch (Exception ex) {
            System.assert(true);
        }

    }

    private class WeatherData extends Observable {
        public Decimal temperature {private set; public get;}
        public Decimal humidity {private set; public get;}
        public Decimal pressure {private set; public get;}

        public void measurementChanged() {

            Map<String, Object> arguments = new Map<String, Object>{'ForecastNoPressure' => true};

            this.setChanged();
            this.notifyObservers(arguments);
        }

        public void setMeasurements(Decimal temperature, Decimal humidity, Decimal pressure) {

            this.temperature = temperature;
            this.humidity = humidity;
            this.pressure = pressure;

            this.measurementChanged();
        }
    }

    private class CurrentConditions implements IObserver {

        private Observable obs; // keep this in case we want to remove our self from the observers
        private Decimal temperature;
        private Decimal humidity;
        private Decimal pressure;

        public CurrentConditions(Observable obs) {
            this.obs = obs;
            obs.addObserver(this);
        }

        public void notify(Observable obs, Map<String, Object> arguments) {

            if (obs instanceof WeatherData) {
                WeatherData weather = (WeatherData) obs;
                this.temperature = weather.temperature;
                this.humidity = weather.humidity;
                this.pressure = weather.pressure;
            }

        }

    }

    private class Forecast implements IObserver {

        private Observable obs; // keep this in case we want to remove our self from the observers
        public Decimal temperature;
        public Decimal humidity;
        public Decimal pressure;

        public Forecast(Observable obs) {
            this.obs = obs;
            obs.addObserver(this);
        }

        public void notify(Observable obs, Map<String, Object> arguments) {

            if (obs instanceof WeatherData) {
                WeatherData weather = (WeatherData)obs;
                this.temperature = weather.temperature;
                this.humidity = weather.humidity;
                this.pressure = weather.pressure;

                if (arguments.get('ForecastNoPressure') == true) {
                    this.pressure = null;
                }

            }

        }

    }
}