/**
 * @author danb@nebulaconsulting.co.uk
 * @date 04/10/2018
 * @description This class represents an observable object. It can be subclassed to represent an object that the
 * application wants to have observed. An observable object can have one or more observers. An observer may be any
 * object that implements interface IObserver. After an observable instance changes, an application calling the
 * Observables notifyObservers method causes all of its observers to be notified of the change by a call to their
 * notify method. See ObservableTest class for an example.
 * Further example here: https://subscription.packtpub.com/book/application-development/9781782173656/4/ch04lvl1sec38/the-observer-pattern
 */

public virtual class Observable {

    private Boolean changed = false;
    private Map<String, IObserver> observers = new Map<String, IObserver>();

    /**
    * @description Marks this Observable object as having been changed
    */
    protected public void setChanged() {
        changed = true;
    }

    /**
    * @description Tests if this object has changed.
    * @return true if changed, false otherwise
    */
    public Boolean hasChanged() {
        return changed;
    }

    /**
    * @description Indicates that this object has no longer changed, or that it has already notified all of its
    * observers of its most recent change, so that the hasChanged method will now return false.
    */
    protected public void clearChanged() {
        changed = false;
    }

    /**
    * @description If this object has changed, as indicated by the hasChanged method, then notify all of its observers
    * and then call the clearChanged method to indicate that this object has no longer changed.
    * @param arguments argument map
    */
    public void notifyObservers(Map<String, Object> arguments) {

        if (this.hasChanged()) {
            // notify each observer in turn
            for (IObserver observer : observers.values()) {
                observer.notify(this, arguments);
            }

            this.clearChanged();
        }
    }

    /**
    * @description Adds an observer to the set of observers for this object, provided that it is not the same as some
    * observer already in the set. Throws NullPointerException if the object parameter is null
    * @param observer observer object that implements interface IObserver
    */
    public void addObserver(IObserver observer) {

        if (observer == null) {
            throw new NullPointerException();
        }

        if (observers.containsKey(getObserverKey(observer)) == false) {
            observers.put(getObserverKey(observer), observer);
        }
    }

    /**
    * @description Clears the observer set so that this object no longer has any observers.
    */
    public void clearObservers() {
        observers.clear();
    }

    /**
    * @description Deletes an observer from the set of observers of this object.
    * @param observer observer object that implements interface IObserver
    */
    public void removeObserver(IObserver observer) {
        if (observer != null && observers.containsKey(getObserverKey(observer))) {
            observers.remove(getObserverKey(observer));
        }
    }

    /**
    * @description Returns the number of observers of this Observable object.
    * @return number of observers
    */
    public Integer countObservers() {
        return observers.size();
    }

    /**
    * @description get observer key from the observer object
    * @return get the observer key from the concrete implementation
    */
    private String getObserverKey(IObserver observer) {
        return String.valueOf(observer).substring(0, String.valueOf(observer).indexOf(':'));
    }

}